//
//  ViewController.swift
//  WeatherCities
//
//  Created by Yaroslav Bykov on 22.11.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MainViewController: UIViewController {

    @IBOutlet private var mainTable: UITableView!
    @IBOutlet private var cityLabel: UILabel!
    @IBOutlet private var iconToday: UIImageView!
    @IBOutlet private var infoLabel: UILabel!
    @IBOutlet private var temperatureLabel: UILabel!
    @IBOutlet private var pressureLabel: UILabel!
    @IBOutlet private var humidityLabel: UILabel!
    @IBOutlet private var windspeedLabel: UILabel!
    @IBOutlet private var iconDegree: UIImageView!
    
    private var weatherCells: [WeatherStruct] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        //loadData(id: <#String#>)
    }
    
    @IBAction func buttonClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.show(vc, sender: nil)
    }
    
    
    func loadData(id: String) {
        let url = Config.url + id + Config.appId
        Alamofire.request(url, method: .get).validate().responseJSON {
            response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                for i in 0..<json["cnt"].intValue {
                    if json["list"][i]["dt_txt"].stringValue.hasSuffix("12:00:00") {
                        let temperatureModel = WeatherStruct(city: json["city"]["name"].stringValue,
                                                             date: json["list"][i]["dt_txt"].stringValue,
                                                             mainInfo: json["list"][i]["weather"][0]["main"].stringValue,
                                                             description: json["list"][i]["weather"][0]["description"].stringValue,
                                                             icon: json["list"][i]["weather"][0]["icon"].stringValue,
                                                             temperature: json["list"][i]["main"]["temp"].stringValue,
                                                             pressure: json["list"][i]["main"]["pressure"].stringValue,
                                                             humidity: json["list"][i]["main"]["humidity"].stringValue,
                                                             windSpeed: json["list"][i]["wind"]["speed"].stringValue,
                                                             windDegree: json["list"][i]["wind"]["deg"].stringValue)
                        self.weatherCells.append(temperatureModel)
                    }
                }
                DispatchQueue.main.async {
                    self.mainTable.reloadData()
                    self.fillView()
                }
                
            case .failure:
                print("Data error")
            }
        }
    }
    
    func fillView() {
        cityLabel.text = weatherCells.first?.city
        infoLabel.text = (weatherCells.first?.mainInfo ?? "") + ", " + (weatherCells.first?.description ?? "")
        temperatureLabel.text = "Temperature: " + (weatherCells.first?.temperature ?? "---") + "C"
        pressureLabel.text = "Pressure: " + (weatherCells.first?.pressure ?? "---") + "HPa"
        humidityLabel.text = "Humidity: " + (weatherCells.first?.humidity ?? "---") + "%"
        windspeedLabel.text = "Windspeed: " + (weatherCells.first?.windSpeed ?? "---") + "m/s"
        iconDegree.image = UIImage(named: "IconWind")
        iconDegree.transform = CGAffineTransform(rotationAngle: CGFloat(Float(weatherCells.first?.windDegree  ?? "0.0")!))
        
        let fullIconUrl = URL(string: (Config.urlIcon + self.weatherCells.first!.icon + ".png"))
        if let data = try? Data(contentsOf: fullIconUrl!) {
            self.iconToday.image = UIImage(data: data)
        } else {
            self.iconToday.image = UIImage(named: "Ooops")
        }
    }

}

    // MARK: - TableView -

extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainCell", for: indexPath) as? MainCell
        
        let fullIconUrl = URL(string: Config.urlIcon + self.weatherCells[indexPath.row].icon + ".png")
        if let data = try? Data(contentsOf: fullIconUrl!) {
            cell?.icon.image = UIImage(data: data)
        } else {
            cell?.icon.image = UIImage(named: "Ooops")
        }
        
        cell?.dateLabel.text = String(self.weatherCells[indexPath.row].date.prefix(10))
        cell?.temperatureLabel.text = "Temp: " + self.weatherCells[indexPath.row].temperature + "C"
        return cell!
    }
    
}
