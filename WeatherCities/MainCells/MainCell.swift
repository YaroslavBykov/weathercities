//
//  MainCell.swift
//  WeatherCities
//
//  Created by Yaroslav Bykov on 22.11.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import UIKit

class MainCell: UITableViewCell {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var temperatureLabel: UILabel!
    
}
