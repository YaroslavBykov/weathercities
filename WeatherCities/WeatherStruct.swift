//
//  WeatherStructure.swift
//  WeatherCities
//
//  Created by Yaroslav Bykov on 22.11.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import Foundation

struct WeatherStruct: Codable {
    let city: String
    let date: String
    let mainInfo: String
    let description: String
    let icon: String
    let temperature: String
    let pressure: String
    let humidity: String
    let windSpeed: String
    let windDegree: String
}

struct Cities: Codable {
    let city: String
    let country: String
    let cityID: String
}
