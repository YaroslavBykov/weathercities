//
//  Global.swift
//  WeatherCities
//
//  Created by Yaroslav Bykov on 23.11.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import Foundation

struct Config {
    static let url = "https://api.openweathermap.org/data/2.5/forecast?id="
    static let appId = "&units=metric&appid=8b3ca614026eaa6d0f65a7264430b25a"
    static let urlIcon = "https://openweathermap.org/img/w/"
    static let jsonPath = "city.list"
    //524901
    static let defaultCity = "524901"
}
