//
//  StartViewController.swift
//  WeatherCities
//
//  Created by Yaroslav Bykov on 02.12.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    @IBAction func selectCityClick(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.show(vc, sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
