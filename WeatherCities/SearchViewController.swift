//
//  SearchViewController.swift
//  WeatherCities
//
//  Created by Yaroslav Bykov on 22.11.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import UIKit
import SwiftyJSON

final class SearchViewController: UIViewController {
    
    var mainVC: MainViewController?
    var arrayCities: [Cities] = []
    var arrayCachedCities: [Cities] = []
    
    @IBOutlet var searchTable: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancel()
        arrayCachedCities = Cache.get() ?? []
    }
    
    func cancel() {
        searchBar.showsCancelButton = false
    }
    
    
    // -- I used local Json, because in APIs doc there are no possibilties for advanced search the cities. That's why slow speed search city (ping 5-15 sec)
    // -- And Search starts from 5th character also because of that problem
    
    func appendCities(text: String?) -> [Cities]{
        guard let jsonFilePath = Bundle.main.path(forResource: Config.jsonPath, ofType: "json(1)") else {return []}
        var arrayCities: [Cities] = []
        let url = URL(fileURLWithPath: jsonFilePath)
        guard (text?.count)! > 2 else {return []}
        do {
            let data = try Data(contentsOf: url)
            let json = JSON(data)
            guard let count = json.array?.count else {return []}
            for i in 0..<count {
                if json[i]["name"].stringValue.hasPrefix(text!) {
                    let city = Cities(city: json[i]["name"].stringValue,
                                       country: json[i]["country"].stringValue,
                                       cityID: json[i]["id"].stringValue)
                    arrayCities.append(city)
                }
            }
            DispatchQueue.main.async {
                self.searchTable.reloadData()
            }
            
        }
        catch {
            print(error)
        }
        
        return arrayCities
        
    }

    
}

// MARK: - Extensions -

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int {
            if searchBar.showsCancelButton == true {
                return arrayCities.count
            } else {
                return arrayCachedCities.count
            }
        }
        //return arrayCities.count
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.searchTable.dequeueReusableCell(withIdentifier: "RecentResultCell", for: indexPath) as? RecentResultCell
        if searchBar.showsCancelButton == false {
            DispatchQueue.main.async {
                if self.arrayCachedCities .isEmpty != true {
                    cell?.recentLabel.text = self.arrayCachedCities[indexPath.row].city + ", " + self.arrayCachedCities[indexPath.row].country
                    }
                }
            }
        else {
            DispatchQueue.main.async {
                if (self.searchBar.text?.count)! > 4 {
                    cell?.recentLabel.text = self.arrayCities[indexPath.row].city + ", " + self.arrayCities[indexPath.row].country
                } else {
                    cell?.recentLabel.text = self.arrayCachedCities[indexPath.row].city + ", " + self.arrayCachedCities[indexPath.row].country
                }
            }
            
        }
        return cell!
    }
    
    
}

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = self.arrayCities[indexPath.row].cityID
        arrayCachedCities.insert(arrayCities[indexPath.row], at: 0)
        Cache.save(arrayCachedCities)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.show(vc, sender: nil)
        vc.loadData(id: id)
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchTable.reloadData()
        let text = searchBar.text
        self.arrayCities = self.appendCities(text: text)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchTable.reloadData()
        if searchText.count > 4 {
            self.arrayCities = self.appendCities(text: searchText)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        let lastID = arrayCachedCities.last?.cityID ?? Config.defaultCity
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.show(vc, sender: nil)
        vc.loadData(id: lastID)
    }
    
}
