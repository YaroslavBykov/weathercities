//
//  Cache.swift
//  WeatherCities
//
//  Created by Yaroslav Bykov on 06.12.2018.
//  Copyright © 2018 Yaroslav Bykov. All rights reserved.
//

import Foundation

struct Cache {
    static let key = "Cache"
    
    static func save(_ value: [Cities]!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
        print("Cache saved")
    }
    
    static func get() -> [Cities]! {
        var citiesData: [Cities]!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            citiesData = try? PropertyListDecoder().decode([Cities].self, from: data)
            return citiesData!
        } else {
            return citiesData
        }
    }
}
